package com.modul5;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.modul5.R;

public class DetailActivity extends AppCompatActivity {
    TextView isi, judul, author;

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setupSharedPreferences();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setTitle(getIntent().getStringExtra("judul"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        judul = findViewById(R.id.detailJudul);
        author = findViewById(R.id.txAuthor);
        isi = findViewById(R.id.detailDesc);
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean("bigSize", false)) {
            judul.setTextSize(getResources().getDimension(R.dimen.big) + 12);
            author.setTextSize(getResources().getDimension(R.dimen.big)-18);
            isi.setTextSize(getResources().getDimension(R.dimen.big)-18);
        } else {
            judul.setTextSize(getResources().getDimension(R.dimen.small) + 12);
            author.setTextSize(getResources().getDimension(R.dimen.small)-12);
            isi.setTextSize(getResources().getDimension(R.dimen.small)-10);
        }
        judul.setText(getIntent().getStringExtra("judul"));
//        judul.setText(prefs.getBoolean("bigSize", false)+""+getResources().getDimension(R.dimen.small));
        isi.setText(getIntent().getStringExtra("isi"));
        author.setText(getIntent().getStringExtra("author") + " | " + getIntent().getStringExtra("date"));
    }

    private void setupSharedPreferences() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        toggleTheme(prefs.getBoolean("nightMode", false));

    }

    public void toggleTheme(Boolean bo) {
        if (bo) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }

    }
}

